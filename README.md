# zml-game-center

### 介绍
游戏中心 —— 制作的小游戏合集，供学习、娱乐、摸鱼使用。 其中包含《贪吃蛇》《吃豆人》《俄罗斯方块》等

### 游戏介绍
#### 1. zml-game-advert         广告小游戏合集，主要就是抖音中小游戏实现
    (1) graden-water  《花园灌溉》
    (2) help-pig  《救救那只猪》
    (3) slide-clean    《方块消除》
#### 2. zml-game-beast          《斗兽棋》
#### 3. zml-game-bird           《管道小鸟》
#### 4. zml-game-gobang         《五子棋》
#### 5. zml-game-pacman-canvas  《吃豆人canvas版》
#### 6. zml-game-pacman         《吃豆人vue版》
#### 7. zml-game-snake          《贪吃蛇》
#### 8. zml-game-tetris         《俄罗斯方块》


